# ESP32 AS COMMUNICATION INTERFACE BETWEEN THE CLOUD AND STM32

This esp32 is programmed to act as a communication interface between the stm32 card and the cloud server

### requirements

* ESP-WROOM-32
* server access certificates

### notas

- connect GPIO16 and GPIO17 pins to PC12 and PD2 pins to establish uart communication

- join the grounds and connect the ESP32 and the STM with their respective power cables

- configure the esp environment according to your computer and where you have it installed

### settings

we configure the wifi network and the password to connect to the internet

```c
    #define EXAMPLE_ESP_WIFI_SSID               "FAMILIA REDIN"
    #define EXAMPLE_ESP_WIFI_PASS               "59669771"
```

We configure the certificates and the direction to access the server 

```c
    #define CLIENT_SN                                   "1004618386"
    #define CLIENT_ID                                   "unal_"CLIENT_SN
    extern const uint8_t aws_root_ca_pem_start[]        asm("_binary_unal_root_CA_crt_start");
    extern const uint8_t aws_root_ca_pem_end[]          asm("_binary_unal_root_CA_crt_end");

    extern const uint8_t client_cert_pem_start[]        asm("_binary_unal_1004618386_certificate_pem_crt_start");
    extern const uint8_t client_cert_pem_end[]          asm("_binary_unal_1004618386_certificate_pem_crt_end");

    extern const uint8_t client_key_pem_start[]         asm("_binary_unal_1004618386_private_pem_key_start");
    extern const uint8_t client_key_pem_end[]           asm("_binary_unal_1004618386_private_pem_key_end");
```

we configure the mklist for the certificates

```
    target_add_binary_data(${CMAKE_PROJECT_NAME}.elf "main/certs/unal-root-CA.crt" TEXT)
    target_add_binary_data(${CMAKE_PROJECT_NAME}.elf "main/certs/unal_1004618386-private.pem.key" TEXT)
    target_add_binary_data(${CMAKE_PROJECT_NAME}.elf "main/certs/unal_1004618386-certificate.pem.crt" TEXT)
```

```
    COMPONENT_EMBED_TXTFILES := certs/unal-root-CA.crt certs/unal_1004618386-certificate.crt certs/unal_1004618386-private.pem.key
```